<?php
declare(strict_types=1);

namespace AppBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class AppBundleExtension extends Extension
{
    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $reflection = new \ReflectionClass(get_class($this));
        $dir        = dirname($reflection->getFileName()) . '/../Resources/config';

        $fileLocator = new FileLocator($dir);

        $loader = new Loader\YamlFileLoader($container, $fileLocator);
        $loader->load("./services.yml");
    }
}
