<?php
declare(strict_types=1);

namespace AppBundle\Transformer;

use AppBundle\Entity\Question;

class QuestionTransformer
{
    /** @var  AnswerTransformer */
    protected $answerTransformer;

    public function __construct(AnswerTransformer $answerTransformer)
    {
        $this->answerTransformer = $answerTransformer;
    }

    public function transform(Question $question, $withAllAnswers = false): array
    {
        $questionArray = [
            'id' => $question->getId(),
            'content' => $question->getContent(),
            'createdAt' => $question->getCreateAt()->getTimestamp(),
        ];

        if ($withAllAnswers) {
            $questionArray['answers'] = $this->answerTransformer->transformList($question->getAnswers()->toArray());
        }

        return $questionArray;
    }

    public function transformList(array $questions, $withAllAnswers = false): array
    {
        $list = [];
        foreach ($questions as $question) {
            $list[] = $this->transform($question, $withAllAnswers);
        }
        return $list;
    }
}
