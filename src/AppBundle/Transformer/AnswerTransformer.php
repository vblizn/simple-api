<?php
declare(strict_types=1);

namespace AppBundle\Transformer;

use AppBundle\Entity\Answer;

class AnswerTransformer
{
    public function transform(Answer $answer): array
    {
        return [
            'id' => $answer->getId(),
            'question' => [
                'id' => $answer->getQuestion()->getId(),
            ],
            'isCorrect' => $answer->isCorrect(),
            'content' => $answer->getContent(),
            'createdAt' => $answer->getCreateAt()->getTimestamp(),
        ];
    }

    public function transformList(array $answers): array
    {
        $answersArray = [];
        foreach ($answers as $answer) {
            $answersArray[] = $this->transform($answer);
        }
        return $answersArray;
    }
}
