<?php
declare(strict_types=1);

namespace AppBundle\Request;

use AppBundle\Entity\Question;
use Symfony\Component\Validator\Constraints as Assert;

class CreateQuestionRequest extends AbstractAppRequest
{
    public function rules()
    {

        return new Assert\Collection(self::getSchema());
    }

    public static function getSubject(): string
    {
        return Question::class;
    }

    public static function getSchema()
    {
        $answerSchema['fields'] = CreateAnswerRequest::getSchema();
        unset($answerSchema['fields']['question']);
        return [
            'content' => [
                new Assert\Type('string'),
                new Assert\NotNull(),
                new Assert\Length(['min' => 20, 'max' => 5000])
            ],
            'answers' => [
                new Assert\Collection([
                    new Assert\Collection($answerSchema),
                    new Assert\Count(['min' => 0, 'max' => 2]),
                ]),
            ],
        ];
    }
}
