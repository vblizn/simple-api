<?php
declare(strict_types=1);

namespace AppBundle\Request;

use Symfony\Component\Validator\Constraints as Assert;

class CreateQuestionListRequest extends CreateQuestionRequest
{
    public function rules()
    {
        return new Assert\Collection([
            parent::rules(),
            new Assert\Count(['min' => 1])
        ]);
    }
}
