<?php
declare(strict_types=1);

namespace AppBundle\Request;

use Fesor\RequestObject\ErrorResponseProvider;
use Fesor\RequestObject\RequestObject;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use AppBundle\Exception\CreateResourceException;

abstract class AbstractAppRequest extends RequestObject implements ErrorResponseProvider
{
    abstract public static function getSubject():string;

    public function getErrorResponse(ConstraintViolationListInterface $errors)
    {
        throw (new CreateResourceException())
            ->setSubject(static::getSubject())
            ->setPayload($this->all())
            ->setErrors($errors);
    }
}
