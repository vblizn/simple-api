<?php
declare(strict_types=1);

namespace AppBundle\Request;

use AppBundle\Entity\Answer;
use Symfony\Component\Validator\Constraints as Assert;

class CreateAnswerRequest extends AbstractAppRequest
{
    public function rules()
    {
        return new Assert\Collection(self::getSchema());
    }

    public static function getSchema()
    {
        return [
            'content' => [
                new Assert\Type('string'),
                new Assert\NotNull(),
                new Assert\Length(['min' => 20, 'max' => 5000])
            ],
            'question' => new Assert\Collection([
                'fields' => [
                    'id' => [
                        new Assert\Type('int'),
                        new Assert\NotBlank(),
                        new Assert\NotNull(),
                    ]
                ],
                'allowExtraFields' => true,
                'allowMissingFields' => false,
            ]),
            'isCorrect' => [
                new Assert\Type('bool'),
                new Assert\NotNull(),
                new Assert\NotNull(),
            ],
        ];
    }

    public static function getSubject(): string
    {
        return Answer::class;
    }
}
