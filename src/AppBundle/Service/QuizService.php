<?php
declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Entity\Answer;
use AppBundle\Exception\CreateResourceException;
use AppBundle\Request\CreateAnswerRequest;
use AppBundle\Request\CreateQuestionListRequest;
use AppBundle\Request\CreateQuestionRequest;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Question;
use AppBundle\Manager\AnswerManager;
use AppBundle\Manager\QuestionManager;

class QuizService
{
    protected $questionManager;
    protected $answerManager;
    protected $entityManager;

    public function __construct(
        QuestionManager $questionManager,
        AnswerManager $answerManager,
        EntityManagerInterface $entityManager
    ) {
        $this->questionManager = $questionManager;
        $this->answerManager = $answerManager;
        $this->entityManager = $entityManager;
    }

    public function createAnswerByRequest(CreateAnswerRequest $request): Answer
    {
        $questionPayload = $request->get('question');
        $question = $this->questionManager
            ->find($questionPayload['id']);

        if (!$question) {
            throw (new CreateResourceException())
                ->setSubject(Answer::class)
                ->setPayload($questionPayload)
                ->setErrors(['Question not found']);
        }

        $answer = $this->createAnswerByDataAndQuestion($request->all(), $question);
        $this->questionManager->addAnswer($question, $answer);

        $this->entityManager->flush();

        return $answer;
    }

    public function createAnswerByDataAndQuestion(array $data, Question $question)
    {
        $data['question'] = $question;
        return $this->answerManager->createByArray($data);
    }

    public function createQuestionListByRequest(CreateQuestionListRequest $request)
    {
        $questions = $request->all();
        foreach ($questions as $question) {
            $createQuestionRequest = new CreateQuestionRequest();
            $createQuestionRequest->setPayload($question);
            $this->createQuestionByRequest($createQuestionRequest);
        }
    }

    public function createQuestionByRequest(CreateQuestionRequest $request): Question
    {
        $questionData = ['content' => $request->get('content')];

        try {
            $question = $this->questionManager->createByArray($questionData);
        } catch (CreateResourceException $exception) {
            $exception->setPayload($request->all());
            throw $exception;
        }

        $answersData = $request->get('answers', []);
            foreach ($answersData as $answerData) {
            $answersErrors = [];
            try {
                $answerData['question'] = $question;
                $answer = $this->answerManager->createByArray($answerData);
                $question->addAnswer($answer);
            } catch (CreateResourceException $exception) {
                $answersErrors[] = $exception->getMessageString(false);
            }
        }

        if (!empty($answersErrors)) {
            throw (new CreateResourceException())->setErrors($answersErrors);
        }

        $this->entityManager->flush();

        return $question;
    }
}
