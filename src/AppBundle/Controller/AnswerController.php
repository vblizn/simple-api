<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Entity\Answer;
use AppBundle\Request\CreateAnswerRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/answer")
 */
class AnswerController extends FOSRestController
{
    /**
     * @Rest\Route("/{id}", methods={"GET"}, name="api.answer.view", requirements={"id"="\d+"})
     * @ParamConverter("answer", class="AppBundle:Answer")
     */
    public function getAnswerAction(Answer $answer)
    {
        return $this->get('app.transformer.answer')->transform($answer);
    }

    /**
     * @Rest\Route("", methods={"POST"}, name="api.answer.create")
     */
    public function createAnswerAction(CreateAnswerRequest $request)
    {
        $answer = $this->get('app.service.quiz')->createAnswerByRequest($request);

        return $this->getAnswerAction($answer);
    }
}
