<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Entity\Question;
use AppBundle\Request\CreateAnswerRequest;
use AppBundle\Request\CreateQuestionRequest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/question")
 */
class QuestionController extends FOSRestController
{
    /**
     * @Route("/{id}", methods={"GET"}, name="api.question.view", requirements={"id"="\d+"})
     * @ParamConverter("question", class="AppBundle:Question")
     */
    public function getQuestionAction(Question $question)
    {
        return $this->get('app.transformer.question')->transform($question, true);
    }

    /**
     * @Route("", methods={"GET"})
     */
    public function getQuestionListAction()
    {
        return $this->get('app.transformer.question')->transformList($this->getManager()->getAll());
    }

    /**
     * @Route("/withAnswer", methods={"GET"})
     */
    public function getQuestionListWithAnswerAction()
    {
        return $this->get('app.transformer.question')->transformList($this->getManager()->getAll(), true);
    }

    /**
     * @Route("/{id}/answer", methods={"GET"}, requirements={"id"="\d+"})
     * @ParamConverter("question", class="AppBundle:Question")
     */
    public function getQuestionAnswerListAction(Question $question)
    {
        $this->get('app.transformer.answer')->transformList($question->getAnswers()->toArray());
    }

    /**
     * @Route("", methods={"POST"})
     */
    public function createQuestionAction(CreateQuestionRequest $request)
    {
        $question = $this->get('app.service.quiz')->createQuestionByRequest($request);
        return $this->getQuestionAction($question);
    }

    /**
     * @Route("/{id}/answer", methods={"POST"}, requirements={"id"="\d+"})
     * @ParamConverter("question", class="AppBundle:Question")
     */
    public function createQuestionAnswerAction(CreateAnswerRequest $request, Question $question)
    {
        $request->set('question', ['id' => $question->getId()]);
        return $this->get('app.controller.answer')->createAnswerAction($request);
    }

    private function getManager()
    {
        return $this->get('app.manager.question');
    }
}
