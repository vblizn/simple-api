<?php
declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Request\CreateQuestionListRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @Rest\Route("/quiz")
 */
class QuizController extends FOSRestController
{
    /**
     * @Rest\Route("", methods={"POST"})
     */
    public function createQuizAction(CreateQuestionListRequest $request)
    {
        $this->get('app.service.quiz')->createQuestionListByRequest($request);
        return $this->get('app.controller.question')->getQuestionListWithAnswerAction();
    }
}
