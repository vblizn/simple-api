<?php
declare(strict_types=1);

namespace AppBundle\Exception;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class CreateResourceException extends \Exception
{
    protected $subject = '';
    protected $payload = [];
    protected $errors = [];

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;
        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setPayload(array $payload): self
    {
        $this->payload = $payload;
        return $this;
    }

    public function getPayload(): array
    {
        $this->payload;
    }

    public function setErrors($errors): self
    {
        if (!(is_array($errors) xor ($errors instanceof ConstraintViolationListInterface))) {
            throw new \LogicException('Wrong error description type given'.var_export($errors, true));
        }

        $this->errors = $errors;
        $this->message = $this->getMessageString();
        return $this;
    }

    public function getErrors(): \ArrayAccess
    {
        return $this->errors;
    }

    public function getMessageString($withHeader = true): string
    {
        $message = '';
        if ($withHeader) {
            $message = sprintf('Wrong data "%s" given for "%s" context:', json_encode($this->payload), $this->subject);
            $message .= "\n";
        }
        $errorsStringArr = [];
        foreach ($this->errors as $key => $error) {
            if ($error instanceof ConstraintViolationInterface) {
                $errorsStringArr[] = sprintf('path: %s; message: %s', $error->getPropertyPath(), $error->getMessage());
            } else {
                $errorsStringArr[] = (string) $error;
            }
        }

        $message .= implode("\n", $errorsStringArr);
        return $message;
    }

    public function __toString(): string
    {
        return $this->getMessageString();
    }
}
