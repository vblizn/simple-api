<?php
declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="question")
 * @ORM\Entity
 */
class Question
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Length(min="20", max="5000")
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content = '';

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Answer", mappedBy="question")
     * @Assert\Count(min="0", max="2", maxMessage="Question may contain up to 2 answers")
     */
    private $answers;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createAt;


    private function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->createAt = $this->createAt??(new \DateTime('now'));
    }

    public static function createByArray(array $data): self
    {
        $obj = new self;
        $obj->content = $data['content'];
        return $obj;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer): self
    {
        $this->answers->add($answer);
        return $this;
    }

    public function getCreateAt(): \DateTime
    {
        return $this->createAt;
    }

    public function __clone()
    {
        $this->id = null;
    }
}
