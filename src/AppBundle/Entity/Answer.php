<?php
declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="answer")
 * @ORM\Entity
 */
class Answer
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Length(min="20", max="5000")
     * @ORM\Column(type="text", nullable=false)
     */
    private $content = '';

    /**
     * @Assert\NotNull()
     * @Assert\Type("bool")
     * @ORM\Column(name="is_correct", type="boolean", nullable=false)
     */
    private $isCorrect = false;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createAt;

    private function __construct()
    {
        $this->createAt = $this->createAt??(new \DateTime('now'));
    }

    public static function createByArray(array $data): self
    {
        $obj = new self;
        $obj->content = $data['content'];
        $obj->question = $data['question'];
        $obj->isCorrect = $data['isCorrect'];

        return $obj;
    }

    public function update(self $answer): self
    {
        $this->content = $answer->content;
        $this->isCorrect = $answer->isCorrect;
        $this->question = $answer->question;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getQuestion(): Question
    {
        return $this->question;
    }

    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    public function getCreateAt(): \DateTime
    {
        return $this->createAt;
    }

    public function __clone()
    {
        $this->id = null;
    }
}
