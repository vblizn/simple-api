<?php
declare(strict_types=1);

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Answer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AnswerFixtures extends Fixture implements OrderedFixtureInterface
{
    const REFERENCE_PREFIX = 'answer.';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i=1;$i<=10;$i++) {
            $question = $this->getReference(QuestionFixtures::REFERENCE_PREFIX . $i);
            $answerNumber = $faker->numberBetween(0,2);
            for ($a=1;$a<=$answerNumber;$a++) {
                $answerLength = $faker->numberBetween(20, 5000);
                $answer = Answer::createByArray([
                    'content' => $faker->text($answerLength),
                    'question' => $question,
                    'isCorrect' => $faker->boolean(),
                        ]);
                $manager->persist($answer);
                $question->addAnswer($answer);
                $this->addReference(QuestionFixtures::REFERENCE_PREFIX . $i . '.' . self::REFERENCE_PREFIX . $a, $answer);
            }
        }
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 15;
    }
}
