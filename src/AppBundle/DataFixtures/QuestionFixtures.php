<?php
declare(strict_types=1);

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Question;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class QuestionFixtures extends Fixture implements OrderedFixtureInterface
{
    const REFERENCE_PREFIX = 'question.';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i=1;$i<=10;$i++) {
            $questionLength = $faker->numberBetween(20, 5000);
            $question = Question::createByArray(['content' => $faker->text($questionLength)]);
            $manager->persist($question);
            $this->addReference(self::REFERENCE_PREFIX . $i, $question);
        }
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10;
    }
}
