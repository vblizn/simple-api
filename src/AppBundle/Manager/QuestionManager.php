<?php
declare(strict_types=1);

namespace AppBundle\Manager;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Exception\CreateResourceException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class QuestionManager
{
    /** @var  EntityManagerInterface */
    protected $entityManager;

    /** @var ValidatorInterface $validator */
    protected $validator;

    /** @var AnswerManager */
    protected $answerManager;


    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        AnswerManager $answerManager
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function getAll()
    {
        return $this->getRepository()->findAll();
    }

    public function createByArray(array $data)
    {
        try {
            $question = Question::createByArray($data);
            $this->validate($question);
        } catch (CreateResourceException $exception) {
            $exception->setPayload($data);
            throw $exception;
        }

        $this->entityManager->persist($question);

        return $question;
    }

    public function addAnswer(Question $question, Answer $answer)
    {
        $question->addAnswer($answer);
        $this->validate($question);

        return $question;
    }


    public function find(int $id): ?Question
    {
        return $this->getRepository()->find($id);
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository(Question::class);
    }

    protected function validate(Question $question)
    {
        $errors = $this->validator->validate($question);

        if (!empty($errors->count())) {
            throw (new CreateResourceException())
                ->setSubject(Answer::class)
                ->setErrors($errors);
        }
    }
}
