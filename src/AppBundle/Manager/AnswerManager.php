<?php
declare(strict_types=1);

namespace AppBundle\Manager;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Exception\CreateResourceException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AnswerManager
{
    /** @var  EntityManagerInterface */
    protected $entityManager;

    /** @var  ValidatorInterface */
    protected $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function createByRequest(Request $request, Question $question = null): Answer
    {
        return $this->createByArray([
            'content' => $request->request->get('content', ''),
            'isCorrect' => $request->get('isCorrect', null),
        ], $question);
    }

    public function createByArray(array $data): ?Answer
    {
        try {
            $answer = Answer::createByArray($data);
            $this->validate($answer);
        } catch (CreateResourceException $exception) {
            $exception->setPayload($data);
            throw $exception;
        }

        $this->entityManager->persist($answer);

        return $answer;
    }

    protected function validate(Answer $answer)
    {
        $errors = $this->validator->validate($answer);

        if (!empty($errors->count())) {
            throw (new CreateResourceException())
                ->setSubject(Answer::class)
                ->setErrors($errors);
        }
    }
}
