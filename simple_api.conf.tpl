server {
	listen 80;
	server_name sa.local www.sa.local;
	root /var/www/simple-api;
	rewrite ^/app\.php/?(.*)$ /$1 permanent;
	

	try_files $uri @rewriteapp;

        location @rewriteapp {
                rewrite ^(.*)$ /app.php/$1 last;
        }

        # Deny all . files
        location ~ /\. {
                deny all;
        }

        location ~ ^/(app|app_dev)\.php(/|$) {
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                include fastcgi_params;
                fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_index app_dev.php;
                send_timeout 1800;
                fastcgi_read_timeout 1800;
            		fastcgi_pass unix:/var/run/php71-fpm.sock;
	}
}
