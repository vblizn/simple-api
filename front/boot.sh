#!/usr/bin/env bash
set -ef

cp /etc/nginx/conf.d/default.conf.tpl /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;'
