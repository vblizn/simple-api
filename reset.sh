#!/bin/bash

rm -rf ./vendor/* && rm -f composer.lock \
 && rm -f ./app/config/parameters.yml
php -r "readfile('https://getcomposer.org/installer');" | php
php composer.phar install
./bin/console do:da:dr --if-exists --force \
  && ./bin/console do:da:cr \
  && ./bin/console do:sc:cr
./bin/console doctrine:fixtures:load

echo "API application ready"
