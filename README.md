#Simple API

Test assignment with implementation of the simple REST API for quiz/tests platform.

#System requirements

This application provides to options to be installed:

### Installation on the host system the same
- PHP 7.1
- MySQL 5.6

### Docker install
- Docker
- Docker compose

#Installation process
## Common step
- Clone this git repository
### Using docker
- checkout the .env file and update if necessary the PHP_HTTP_PORT of the frontend container that will be mapped outside;
- run 
```bash
docker-compose up --build
```
You need nothing else. When you see 

```sh
API application ready
```

that means that test application is ready to use and is available on the mapped port.

### Using host system configuration.

- given you have all the necessary dependecies installed and configured;
- configure you wev server to be able to handle HTTP requests. Example for the nginx web server is provided in the simple_api.conf.tpl file in the root of the project;
- go to the console and run

```sh
chmod +x /{project}/reset.sh
/{project}/reset.sh
```

That's all. Now you are ready to test this app.

#Api endpoints

##Quiz
- POST /api/quiz - batch creating of questions with answers

##Question
- GET /api/question - get questions list with less data
- GET /api/question/withAnswers - get questions list with full data
- GET /api/question/{id} - get question data
- GET /api/question/{id}/answer - get question answers list
- POST /api/question - create question
- POST /api/{id}/answer - add(create) question answer

###Answer
- GET /api/answer/{id} - get answer
- POST /api/answer - create answer


#Request examples

###Batch create questions
~~~~
[
    {
	      "content": "asdfsdfsdfsdfsdfsdfsdfsdfsdfsd",
	      "answers": [
		                  {
		                    "content": "asdddddddddddddddddddddddddddd",
		                    "isCorrect": false
	                    },
		                  {
                        "content": "asddddddddddddddddddddddddddd",
                        "isCorrect": true
	                    }
	      ]
    }
]
~~~~
###Create answer

~~~~
{
    "question": {
         "id": 5
     },
	   "content": "yesfffffffffffffffff",
	   "isCorrect": true
}
~~~~

###Create single question with answers

~~~~
{
    "content": "yesfffffffffffffffff",
	   "answers": [
		    {
		      "content": "asddddddddddddddddddddddd",
		      "isCorrect": false
	      },
		    {
		      "content": "asddddddddddddddddddddddd",
		      "isCorrect": true
	      }
	    ]
}
~~~~


#TODO:
- improve error messages formatting
- add API doc generation
- cover API with tests
- ...sleep more

#Time spent = 10 h
