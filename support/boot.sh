#!/usr/bin/env bash

# Wait for MySQL
while ! mysqladmin ping -h $SYMFONY__DATABASE__HOST -u$MYSQL_USER -p$MYSQL_PASSWORD --silent; do
      sleep 1
done

rm -f /srv/app/config/parameters.yml
cp /srv/app/config/parameters.yml.docker /srv/app/config/parameters.yml

rm -rf ./vendor/* && rm -f composer.lock
php -r "readfile('https://getcomposer.org/installer');" | php
php composer.phar install
./bin/console do:da:dr --if-exists --force \
  && ./bin/console do:da:cr \
  && ./bin/console do:sc:cr
./bin/console doctrine:fixtures:load -q

echo "API application ready"

php-fpm --allow-to-run-as-root
