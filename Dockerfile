FROM php:7.1-fpm

RUN apt-get update && \
    apt-get install -y libmcrypt-dev libpq-dev netcat mysql-client && \
    rm -rf /var/lib/apt/lists/*


RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd && \
    docker-php-ext-configure mysqli --with-mysqli=mysqlnd

RUN docker-php-ext-install \
    pdo_mysql \
    mysqli \
    zip \
    mbstring \
    opcache

RUN printf "\n" | pecl install apcu-beta && echo extension=apcu.so > /usr/local/etc/php/conf.d/10-apcu.ini
RUN printf "\n" | pecl install apcu_bc-beta && echo extension=apc.so > /usr/local/etc/php/conf.d/apc.ini


COPY support/php/fpm_www.conf /usr/local/etc/php-fpm.d/www.conf
COPY . /srv/
COPY ./support/boot.sh /usr/local/bin/boot_app

RUN chmod +x /usr/local/bin/boot_app

WORKDIR /srv

CMD ["/usr/local/bin/boot_app"]
